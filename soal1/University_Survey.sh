#!/bin/bash

echo Rangking 5 besar jepang
awk -F "," '/JP/ {print $1,$2}' 2023_QS_World_University_Rankings.csv | head -5

echo

echo Fsr terendah dari 5 besar di Jepang
awk -F "," -v OFS=' | ' '/JP/ {print $1, $3, $2, $9}' 2023_QS_World_University_Rankings.csv | sort -t "|" -nk 4 | head -5
echo

echo 10 Universitas dengan GER rang tertinggi
awk -F "," -v OFS=' / ' '/JP/{print $1,$2,$20}' 2023_QS_World_University_Rankings.csv | sort -t "/" -nk3 | head -10

echo

echo Universitas Keren
awk -F "," '/Keren/{print $1,$2}' 2023_QS_World_University_Rankings.csv
