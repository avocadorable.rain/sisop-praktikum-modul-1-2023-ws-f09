#/!bin/bash

# buat file dekripsi dengan format nama file
# disini saya menggunakan format namafie

namafile=$(date "+%H":"%M %d":"%m":"%y decrypt")

# kemudian simpan waktu atau jam sekarang pada variabel hour
hour=$(date "+%H")

# buat array untuk menyimpan huruf agar bisa didapat valuenya

#saya menggunakan nama variabel alfabet untuk menyimpan array dengan huruf kecil
alfabet=(a b c d e f g h i j k l m n o p q r s t u v w x y z )

#saya menggunakan nama variabel kapital untuk menyimpan array yang berisi huruf besar
kapital=(A B C D E F G H I J K L M N O P Q R S T U V W X Y Z)

# sandi 1 merupakan string untuk parameter tr yang digunakan untuk menggeser barisan afabet (alfabet)
sandi1="${alfabet[$hour]}-z}a-${alfabet[$hour-1]}"
# dan sandi 2 (kapital) adalah string untuk parameter tr yang menggeser barisan alfabet 
sandi2="${kapital[$hour]}-Z}A-${kapital[$hour-1]}"

# pindah ke directory yang diinginkan untuk menyimpan hasil dekripsi
cd '/home/fauzi/sisop/mod1-s4'
# tuliskan hasil dekripsi ke  $namafile.txt
echo -n "$(cat /home/fauzi/sisop/mod1-s4/20:38\ 06:03:23.txt | tr $sandi1 'a-z' | tr $sandi2 'A-Z')" > "$namafile.txt"


