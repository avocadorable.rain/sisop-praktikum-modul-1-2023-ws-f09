#/!bin/bash


# nama file hasil enkripsi, 
# disini saya menggunakan  nama dengan namafile
filename=$(date "+%H":"%M %d":"%m":"%y")

# variabel untuk mendapatkan jam sekarang dan dimasukkan ke  variabel hour 
hour=$(date "+%H")

# buat array untuk mendapatkan value per hurufnya

#saya menggunakan nama variabel alfabet untuk menyimpan array berisi huruf kecil
alfabet=(a b c d e f g h i j k l m n o p q r s t u v w x y z)


#saya menggunakan nama variabel kapital untuk menyimpan array berisi huruf besar
kapital=(A B C D E F G H I J K L M N O P Q R S T U V W X Y Z)

# sandi 1 merupakan string untuk parameter tr yang digunakan untuk menggeser baris huruf alfabet
sandi1="${alfabet[$hour]}-z}a-${alfabet[$hour-1]}"

# sandi 2 merupakan string untuk parameter rt yang digunakan untuk menggeser baris huruf kapital
sandi2="${kapital[$hour]}-Z}A-${kapital[$hour-1]}"

# simpan hasil enkripsi ke file yang diinginkan
cd '/home/fauzi/sisop/mod1-s4'

#  menyimpan hasil enrktipsi dari string
echo -n "$(cat /var/log/syslog | tr 'a-z' $sandi1 | tr 'A-Z' $sandi2)" > "$filename.txt"

# 0 */2 * * * /bin/bash /home/fauzi/sisop/mod1-s4/log_encrypt.sh
