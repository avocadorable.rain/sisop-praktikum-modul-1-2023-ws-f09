## Anggota Kelompok

Abyan Zhafran Trisnanto // 5025211218

Fauzi Rizki Pratama // 5025211220

Khairuddin Nasty // 5025201041

Rani Listian Anggraeni // 5025211171

### Soal Shift
- [link](https://drive.google.com/drive/folders/1Zof7rySo9QdzmAhfNc1vFMXnIbLM-SRD)

### Soal 1
    Terdapat 4 perintah pada soal pertama
    1. Menampilkan 5 rank teratas universitas di Jepang
        untuk memfilter data diperlukan command 'awk' dengan keyword 'JP'. Kemudian karena hanya 5 data
        menggunakan 'head -5'
    2. menampilkan fsr terendah dari hasil di atas
        maka, command diatas ditambahkan command 'sort' bedasarkan kolom fsr. Kemudian diambil satu saja
        dengan command 'head -1'
    3. Menampilkan 10 universitas teratas bedasarkan ger rank
        Maka, sama dengan perintah pertama, hanya saja dilakukan 'sort' terlebih dahulu, kemudian 'head-10'
    4. Menampilkan universitas dengan kata kunci 'keren'
        sama dengan perintah pertama, hanya mengganti 'JP' menjadi 'Keren'.
### Soal 2
### Soal 3
### Soal 4
