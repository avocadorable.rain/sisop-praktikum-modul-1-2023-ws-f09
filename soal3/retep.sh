#!/bin/bash

# input username dan password
echo "Please enter your username:"
read user
echo "Please enter your password:"
read -s passwd

# mengecek apakah user terdaftar pada file users.txt
if ! grep -q "^$user:" /users/users.txt; then
    echo "User does not exist"
    echo "$(date +'%y/%m/%d %H:%M:%S') LOGIN: ERROR Failed login attempt on user $user" >> log.txt
    exit 1
fi

# mengambil password dari file users.txt
userpasswd=$(grep "^$user:" /users/users.txt | cut -d ":" -f 2)

# mengecek apakah password sudah benar
if [ "$userpasswd" != "$passwd" ]; then
    echo "Incorrect password."
    echo "$(date +'%y/%m/%d %H:%M:%S') LOGIN: ERROR Failed login attempt on user $user" >> log.txt
    exit 1
fi

echo "$(date +'%y/%m/%d %H:%M:%S') LOGIN: INFO User $user logged in" >> log.txt
echo "Welcome, $user!"
exit 0
